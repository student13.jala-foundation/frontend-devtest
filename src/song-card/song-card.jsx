import React, { useState } from "react";
import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
import CardMedia from "@mui/material/CardMedia";
import { CardActionArea } from "@mui/material";
import IconButton from "@mui/material/IconButton";
import PlayArrowIcon from "@mui/icons-material/PlayArrow";

import "./song.scss";

function SongCard(props) {
  const [track] = useState(props.track);

  return (
    <Card sx={{ minWidth: 200, maxWidth: 200 }}>
      <CardActionArea>
        <CardMedia
          component="img"
          height="140"
          image={track.album.cover_medium}
          alt={track.album.title}
        />
        <CardContent classes={{ root: "card_container" }}>
          <p className="title">{track.title}</p>

          <div className="container">
            <p className="artist">{track.artist.name}</p>
            <IconButton aria-label="play/pause">
              <PlayArrowIcon sx={{ height: 30, width: 30 }} />
            </IconButton>
          </div>
        </CardContent>
      </CardActionArea>
    </Card>
  );
}

export default SongCard;
