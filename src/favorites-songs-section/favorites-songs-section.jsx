import React, { useState } from "react";
import "./favorite-songs.scss";

function FavoritesSongsSection() {
  const [user] = useState("Jonh Doe");
  return (
    <div className="user__container">
      <h2 className='user__title'> {user} </h2>
      <div className="user__messages">Your Favorites</div>
    </div>
  );
}

export default FavoritesSongsSection;
