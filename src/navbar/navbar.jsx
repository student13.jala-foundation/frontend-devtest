import React from "react";
import { Link } from "react-router-dom";

import AppBar from "@mui/material/AppBar";
import Box from "@mui/material/Box";
import Toolbar from "@mui/material/Toolbar";
import { Button } from "@mui/material";

function CustomNavbar() {
  return (
    <Box sx={{ flexGrow: 1 }}>
      <AppBar position="static">
        <Toolbar>
          <Button component={Link} to={"/"} color="inherit">
            Home
          </Button>
          <Button component={Link} to={"user"} color="inherit">
            User
          </Button>
          <Button component={Link} to={"music"} color="inherit">
            Music
          </Button>
        </Toolbar>
      </AppBar>
    </Box>
    //     <nav>
    //       <Link to='/'> Home </Link>
    //       <Link to='user'> User </Link>
    //       <Link to='music'> Music </Link>
    //   </nav>
  );
}

export default CustomNavbar;
