import axios from 'axios';

export const musicService = {
    getTracks, 
    getTrackDetail,
    getAlbum
};

function getTracks(){
    return axios.get('https://cors-anywhere.herokuapp.com/https://api.deezer.com/chart/0/tracks');
}

function getTrackDetail(id){
    return  axios.get(`https://cors-anywhere.herokuapp.com/https://api.deezer.com/chart/0/tracks/${id}`);
}

function getAlbum(id){
    return axios.get(`https://api.deezer.com/album/${id}`)
}