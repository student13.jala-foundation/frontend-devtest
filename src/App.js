import { Route, Routes } from 'react-router-dom';
import Home from './home/home';
import MusicSection from './music-section/music-section';
import FavoritesSongsSection from './favorites-songs-section/favorites-songs-section';
import CustomNavbar from './navbar/navbar';

import './App.scss';

function App() {
  return (
    <div className="App">
       {/* <CustomNavbar /> */}
      <Routes>
        <Route path='/' element={<Home />}/>
        <Route path='user' element={<FavoritesSongsSection/> } />
        <Route path='music' element={<MusicSection/>} />
      </Routes>
    </div>
  );
}

export default App;
