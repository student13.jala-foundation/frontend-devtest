import React, { useState, useEffect } from "react";
import { musicService } from "../services/music-service";
import SongCard from "../song-card/song-card";
import './music-section.scss';

function MusicSection() {
  const [music, setMusic] = useState([]);

  useEffect(() => {
    async function fetchData() {
      const response = await musicService.getTracks();
      setMusic(response.data.data);
    }
    fetchData();
  }, []);
  return (
    <div className='track__container'>
      {music.map((track) => {
        return (
          <div key={track.id} className='track__song' >
            <SongCard track={track} />
          </div>
        );
      })}
    </div>
  );
}

export default MusicSection;
