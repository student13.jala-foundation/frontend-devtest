import * as React from "react";
import MusicSection from "../music-section/music-section";
import FavoritesSongsSection from "../favorites-songs-section/favorites-songs-section";
import "./home.scss";

function Home() {
  return (
    <div className="home__container">
      <article className="home__music-container">
        <h2 className="home__title">Music Section</h2>
        <section className="home__music">
          <MusicSection />
        </section>
      </article>
      <section className="home__user">
        <FavoritesSongsSection />
      </section>
    </div>
  );
}

export default Home;
